package com.avatar.randomAvatar.Model.Pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName avatars
 */
@TableName(value ="avatars")
@Data
public class Avatars implements Serializable {
    /**
     * 头像ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 头像地址
     */
    private String position;

    /**
     * 头像性别
     * 1代表男
     * 2代表其他
     * 0代表女
     */
    private Integer gender;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", position=").append(position);
        sb.append(", gender=").append(gender);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}