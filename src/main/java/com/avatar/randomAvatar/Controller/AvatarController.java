package com.avatar.randomAvatar.Controller;

import com.avatar.randomAvatar.service.AvatarsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/avatar")
public class AvatarController {

    @Resource
    AvatarsService avatarsService;

    @GetMapping("/random")
    public void randomGetAvatar(@RequestParam int gender, HttpServletResponse response){
        try {
            avatarsService.getRandomAvatar(gender,response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
