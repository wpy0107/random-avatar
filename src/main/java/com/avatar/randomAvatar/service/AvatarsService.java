package com.avatar.randomAvatar.service;


import com.avatar.randomAvatar.Model.Pojo.Avatars;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
* @author 56372
* @description 针对表【avatars】的数据库操作Service
* @createDate 2023-05-07 16:07:34
*/
public interface AvatarsService extends IService<Avatars> {

    public void getRandomAvatar(int gender, HttpServletResponse response) throws IOException;

}
