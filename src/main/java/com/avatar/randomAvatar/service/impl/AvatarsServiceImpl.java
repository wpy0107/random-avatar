package com.avatar.randomAvatar.service.impl;


import com.avatar.randomAvatar.Model.Pojo.Avatars;
import com.avatar.randomAvatar.mapper.AvatarsMapper;
import com.avatar.randomAvatar.service.AvatarsService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;
import java.util.Random;


/**
* @author 56372
* @description 针对表【avatars】的数据库操作Service实现
* @createDate 2023-05-07 16:07:34
*/
@Service
public class AvatarsServiceImpl extends ServiceImpl<AvatarsMapper, Avatars>
    implements AvatarsService {

    private static final String DEFAULT_MAN_POSITION = "D:\\avatar\\man\\";
    private static final String DEFAULT_WOMEN_POSITION = "D:\\avatar\\women\\";
    private static final String DEFAULT_OTHER_POSITION = "D:\\avatar\\other\\";

    /**
     *  根据gender属性从服务器不同文件夹中下载头像到img文件夹中，之后让前端从img中读取文件
     * */
    @Override
    public void getRandomAvatar(int gender, HttpServletResponse response) throws IOException {
        //根据gender从数据库中查询头像，并随机返回一个头像的文件名
        QueryWrapper<Avatars> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("gender",gender);
        List<Avatars> avatarsList = this.list(queryWrapper);
        int size = avatarsList.size();
        //从集合中随机获取一个头像对象
        Random random = new Random();
        //nextInt(n)获取从0到n（不包括n但包括0）的正整数
        Avatars avatars = avatarsList.get(random.nextInt(size));
        //拿到图片的图片名和性别类型
        String position = avatars.getPosition();
        //根据性别不同，分别取读取不同文件夹中的图片
        //通过输入流读取文件内容,文件名要从数据库中获取


//        File file = null;
//        BufferedImage im = null;
//            switch (gender){
//                case 0:
//                    file = new File(DEFAULT_WOMEN_POSITION+position);
//                    break;
//                case 1:
//                    file = new File(DEFAULT_MAN_POSITION+position);
//                    break;
//                case 2:
//                    file = new File(DEFAULT_OTHER_POSITION+position);
//            }
//            im = ImageIO.read(file);
//            response.setContentType("image/jpeg");
//            ServletOutputStream sos = response.getOutputStream();
//            ImageIO.write(im,"jpeg",sos);

        //通过输入流读取文件内容
        FileInputStream inputStream = null;
        switch (gender){
                case 0:
                    inputStream = new FileInputStream(new File(DEFAULT_WOMEN_POSITION+position));
                    break;
                case 1:
                    inputStream = new FileInputStream(new File(DEFAULT_MAN_POSITION+position));
                    break;
                case 2:
                    inputStream = new FileInputStream(new File(DEFAULT_OTHER_POSITION+position));
            }
        //通过输出流将文件写回浏览器，在浏览器展示图片
        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType("image/jpeg");
        int len = 0;
        byte[] bytes = new byte[1024];
        while ((len=inputStream.read(bytes))!=-1){
            outputStream.write(bytes,0,len);
            outputStream.flush();
        }
        //关闭资源
        outputStream.close();
        inputStream.close();
    }
}




