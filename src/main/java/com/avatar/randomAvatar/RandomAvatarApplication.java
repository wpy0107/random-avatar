package com.avatar.randomAvatar;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.avatar.randomAvatar.mapper")
@SpringBootApplication
public class RandomAvatarApplication {

    public static void main(String[] args) {
        SpringApplication.run(RandomAvatarApplication.class, args);
    }

}
