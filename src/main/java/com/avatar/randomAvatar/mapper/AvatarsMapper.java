package com.avatar.randomAvatar.mapper;


import com.avatar.randomAvatar.Model.Pojo.Avatars;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 56372
* @description 针对表【avatars】的数据库操作Mapper
* @createDate 2023-05-07 16:07:34
*/
public interface AvatarsMapper extends BaseMapper<Avatars> {

}




